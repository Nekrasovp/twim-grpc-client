#!/usr/bin/env python
# encoding: utf-8

import time
import aiohttp
import asyncio
from aiohttp import web

from client import TwimClient
from streams import BalanceStream, DepthOfMarketStream, OrderStream
from common import Executor, dumps

import const

STREAMS = [
    BalanceStream,
    OrderStream
]


async def index(request:web.Request, client:TwimClient):
    body = open('readme.html', 'rb').read()
    return web.Response(text=body.decode('utf-8'), content_type='text/html')

async def get_ballance(request:web.Request, client:TwimClient):
    return web.json_response(tw.getBallance(), dumps=dumps)

async def get_currencies(request:web.Request, client:TwimClient):
    return web.json_response(tw.getCurrencies())

async def get_securities(request:web.Request, client:TwimClient):
    return web.json_response(tw.getSecurities())

async def get_accounts(request:web.Request, client:TwimClient):
    return web.json_response(tw.getAccounts())    

async def fetch_bids_asks(request:web.Request, client:TwimClient):
    data = await request.post()
    symbol = data.get('symbol')
    return web.json_response(tw.getDepthOfMarket(symbol=symbol), dumps=dumps)

async def fetch_order(request:web.Request, client:TwimClient):
    order_id = request.query.get('order_id')
    client_order_id = request.query.get('client_order_id')
    order = client.getOrder(client_order_id=client_order_id, order_id=order_id)
    return web.json_response({'order':order}, dumps=dumps)

async def fetch_orders(request:web.Request, client:TwimClient):
    data = client.getOrders()
    return web.json_response(data, dumps=dumps)

async def fetch_historical_orders(request:web.Request, client:TwimClient):
    data = await request.post()
    count = data.get('count') or 100
    offset = data.get('offset') or 0
    client.loadOrders(Count=int(count), Offset=int(offset))
    data = client.getOrders()
    return web.json_response(data, dumps=dumps)

async def create_order(request:web.Request, client:TwimClient):
    data = await request.post()
    symbol = data.get('symbol')
    side = data.get('side')
    price = data.get('price')
    quantity = data.get('quantity')
    ClientOrderId = data.get('client_order_id')

    order_request = client.orderStreamPlaceRequest(symbol, side, float(price), float(quantity), ClientOrderId=int(ClientOrderId))
    client.order_stream.send(order_request)

    return web.json_response({'client_order_id':ClientOrderId})

async def refresh_order(request:web.Request, client:TwimClient):
    order_id = request.query.get('order_id')
    client_order_id = request.query.get('client_order_id')
    
    order_request = client.orderStreamStatusRequest(order_id, client_order_id)
    client.order_stream.send(order_request)
    
    await asyncio.sleep(.1)

    order = client.getOrder(client_order_id=client_order_id, order_id=order_id)

    return web.json_response({'order':order}, dumps=dumps)

async def cancel_order(request:web.Request, client:TwimClient):
    order_id = request.query.get('order_id')
    client_order_id = request.query.get('client_order_id')
    
    order_request = client.orderStreamCancelRequest(order_id, client_order_id)
    client.order_stream.send(order_request)

    return web.json_response({'order_id':order_id, 'client_order_id':client_order_id}, dumps=dumps)

    
tw = TwimClient(
    const.TWIM_HOST, 
    const.TWIM_ACCOUNT_ID, 
    const.TWIM_PASSWORD,
    # fmt=float
)


@web.middleware
async def middleware(request, handler):
    if 'favicon.ico' in str(request.url):
        return await handler(request)
    return await handler(request, client=tw)


def main():
    
    loop = asyncio.get_event_loop()
    execute = Executor(loop, nthreads=(len(const.SYMBOLS) + len(STREAMS)))

    for stream in STREAMS:
        execute(stream, client=tw)
    
    for symbol in const.SYMBOLS:
        execute(DepthOfMarketStream, client=tw, symbol=symbol)
    
    # tw.loadOrders(Count=const.HISTORICAL_ORDERS_COUNT)
    
    app = web.Application(middlewares=[middleware])

    app.add_routes([
        web.get('/', index),
        web.get('/get_currencies', get_currencies),
        web.get('/get_securities', get_securities),
        web.get('/get_accounts', get_accounts),
        web.get('/get_ballance', get_ballance),
        web.post('/fetch_bids_asks', fetch_bids_asks),
        web.post('/create_order', create_order),
        web.get('/refresh_order', refresh_order),
        web.get('/cancel_order', cancel_order),
        web.get('/fetch_order', fetch_order),
        web.get('/fetch_orders', fetch_orders),
        web.post('/fetch_historical_orders', fetch_historical_orders)
    ])

    try:
        web.run_app(app)
    except KeyboardInterrupt:
        exit(0)

if __name__ == '__main__':
    main()
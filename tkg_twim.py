import tkgcore
import asyncio
from client import TwimClient
from streams import BalanceStream, DepthOfMarketStream, OrderStream
from common import Executor, dumps

import const
import random

STREAMS = [
    BalanceStream,
    OrderStream, DepthOfMarketStream
]


class TkgTwim(tkgcore.ccxtExchangeWrapper):
    def __init__(self, exchange_id, api_key="", secret=""):

        super().__init__("")
        random.seed()

        self.wrapper_id = "twim"

        self.tw = TwimClient(
            const.TWIM_HOST,
            const.TWIM_ACCOUNT_ID,
            const.TWIM_PASSWORD,
            # fmt=float
        )

        self.loop = asyncio.get_event_loop()
        self.execute = Executor(self.loop, nthreads=(len(const.SYMBOLS) + len(STREAMS)))

        for stream in STREAMS:
            self.execute(stream, client=self.tw)

        for symbol in const.SYMBOLS:
            self.execute(DepthOfMarketStream, client=self.tw, symbol=symbol)

        # self.tw.loadOrders(Count=const.HISTORICAL_ORDERS_COUNT)

    def _fetch_balance(self):
        """
        Return balance in ccxt format:
        {"info": {...} , # the original format
        "free":
                {"BTC": ... ,
                ...   }  available free balance
        "used": ... , # used in orders and etc...

        "BTC":  #  per every currency
            {"free": ... , # free amount as amount in twim
            "used": ... , # reserve from twim
            }
         ...
         }
        :return: dict
        """

        balance = None
        while balance is None or len(balance) == 0:
            balance = self.tw.getBallance()

        tkg_bal = dict()
        tkg_bal["free"] = dict()
        tkg_bal["used"] = dict()
        tkg_bal["info"] = balance

        for i, b in balance.items():
            if b["Symbol"] not in tkg_bal:
                tkg_bal["free"][b["Symbol"]] = float(b["amount"])
                tkg_bal["used"][b["Symbol"]] = float(b["reserve"])
                tkg_bal[b["Symbol"]] = {"free": float(b["amount"]), "used": float(b["reserve"])}

        return tkg_bal

    def fetch_free_balance(self):
        return self._fetch_balance()["free"]

    def _load_markets(self):
        markets = None
        while markets is None or len(markets) == 0:
            markets = self.tw.getSecurities()
        return markets

    def _fetch_order_book(self, symbol, limit=None):
        order_book = None
        while order_book is None or len(order_book) == 0:
            order_book = self.tw.getDepthOfMarket(symbol=symbol)

        order_book["bids"] = sorted(order_book['bids'], key=lambda x: x[0], reverse=True)
        order_book["asks"] = sorted(order_book['asks'], key=lambda x: x[0], reverse=False)
        return order_book

    def _fetch_ticker(self, symbol):
        order_book = self._fetch_order_book(symbol)
        ticker = dict()
        ticker[symbol] = dict()
        # place 0 while stream is not initialised, better decision wait for an initialising
        if len(order_book["asks"]) > 0:
            ticker[symbol]["ask"] = order_book["asks"][0][0]
        else:
            ticker[symbol]["asks"] = 0
        if len(order_book["bids"]) > 0:
            ticker[symbol]["bid"] = order_book["bids"][0][0]
        else:
            ticker[symbol]["bid"] = 0

        ticker[symbol]["timestamp"] = order_book["timestamp"]
        return ticker

    @staticmethod
    def rand64():
        a = random.randint(1000000000, 99999999999999999)
        return a

    def _fetch_order_by_client_id(self, client_order_id:int):
        # order_id = request.query.get('order_id')
        client_order_id = client_order_id
        order = None
        while order is None:
            try:
                order = self.tw.getOrder(client_order_id=client_order_id)
            except Exception as e:
                pass
        return order.toJson()

    def _fetch_order_by_order_id(self, order_id:int):
        # order_id = request.query.get('order_id')
        order_id = order_id
        order = None
        while order is None:
            try:
                order = self.tw.getOrder(order_id=order_id)
            except Exception as e:
                pass
        return order.toJson()

    def _create_order(self, symbol, order_type, side, amount, price=None, params: dict=None):
        """
        Returns the resp from exchange with order data. Not all the fields could be returned

            {
                'id':                '12345-67890:09876/54321', // string or int
                'status':     'open',         // 'open', 'closed', 'canceled'
                'symbol':     'ETH/BTC',      // symbol
                'type':       'limit',        // 'market', 'limit'
                'side':       'buy',          // 'buy', 'sell'
                'price':       0.06917684,    // float price in quote currency
                'amount':      1.5,           // ordered amount of base currency
                'filled':      1.1,           // filled amount of base currency
                'remaining':   0.4,           // remaining amount to fill
                'cost':        0.076094524,   // 'filled' * 'price' (filling price used where available)
                'trades':    [ ... ],         // a list of order trades/executions
                'fee': {                      // fee info, if available
                    'currency': 'BTC',        // which currency the fee is (usually quote)
                    'cost': 0.0009,           // the fee amount in that currency
                    'rate': 0.002,            // the fee rate (if available)
                },
                'info': { ... },              // the original unparsed order structure as is
            }

        :param symbol: pair symbol
        :param order_type: "limit"
        :param side:  "buy" or "sell"
        :param amount: amount of order in base currency
        :param price: price in quote currency
        :param params: additional parameters
        :return dict: with exchange update with order data

        """
        client_order_id = self.rand64()
        order_request = self.tw.orderStreamPlaceRequest(symbol, side, float(price), float(amount),
                                                        ClientOrderId=int(client_order_id))

        self.tw.order_stream.send(order_request)
        resp = self._fetch_order_by_client_id(client_order_id)
        # order status to ccxt style 3 order statuses
        # open status
        if resp['order_status'] == 'new' or \
           resp['order_status'] == 'partiallyfilled' or \
           resp['order_status'] == 'pendingcancel' or \
           resp['order_status'] == 'pendingreplace':
            return dict(
                id=resp['order_id'],
                status='open',
                symbol=symbol,
                type='limit',
                side=side,
                price=float(resp['price']),
                amount=float(resp['quantity']),
                filled=float(resp['cum_qty']),
                remaining=float(resp['leaves_quantity']),
                cost=0,
                info=resp
            )
        # TODO Move to another place and wrap as a function
        # canceled status
        elif resp['order_status'] == 'invalid' or \
             resp['order_status'] == 'canceled' or \
             resp['order_status'] == 'rejected' or \
             resp['order_status'] == 'pendingnew' or \
             resp['order_status'] == 'expired':
                return dict(
                    id=resp['order_id'],
                    status='canceled',
                    symbol=symbol,
                    type='limit',
                    side=side,
                    price=float(resp['price']),
                    amount=float(resp['quantity']),
                    filled=float(resp['cum_qty']),
                    remaining=float(resp['leaves_quantity']),
                    info=resp
                )
        # closed status
        if resp['order_status'] == 'filled':
            return dict(
                id=resp['order_id'],
                status='closed',
                symbol=symbol,
                type='limit',
                side=side,
                price=float(resp['price']),
                amount=float(resp['quantity']),
                filled=float(resp['cum_qty']),
                remaining=float(resp['leaves_quantity']),
                cost=float(resp['average_price']),
                info=resp,
                trades=dict(
                    lastprice= float(resp['LastPrice']),
                    lastquantity= float(resp['LastQuantity']),
                    paidcomission= float(resp['PaidComission']))
            )

    def _cancel_order(self, order):
        cancel_request = self.tw.orderStreamCancelRequest(OrderId=order.id)
        self.tw.order_stream.send(cancel_request)
        resp = self._fetch_order_by_order_id(order.id)
        # order status to ccxt style 3 order statuses
        # open status
        if resp['order_status'] == 'new' or \
           resp['order_status'] == 'partiallyfilled' or \
           resp['order_status'] == 'pendingcancel' or \
           resp['order_status'] == 'pendingreplace':
            return dict(
                id=resp['order_id'],
                status='open',
                symbol=order.symbol,
                type='limit',
                side=order.side,
                price=float(resp['price']),
                amount=float(resp['quantity']),
                filled=float(resp['cum_qty']),
                remaining=float(resp['leaves_quantity']),
                cost=0,
                info=resp
            )
        # TODO Move to another place and wrap as a function
        # canceled status
        elif resp['order_status'] == 'invalid' or \
             resp['order_status'] == 'canceled' or \
             resp['order_status'] == 'rejected' or \
             resp['order_status'] == 'pendingnew' or \
             resp['order_status'] == 'expired':
                return dict(
                    id=resp['order_id'],
                    status='canceled',
                    symbol=order.symbol,
                    type='limit',
                    side=order.side,
                    price=float(resp['price']),
                    amount=float(resp['quantity']),
                    filled=float(resp['cum_qty']),
                    remaining=float(resp['leaves_quantity']),
                    info=resp
                )
        # closed status
        if resp['order_status'] == 'filled':
            return dict(
                id=resp['order_id'],
                status='closed',
                symbol=order.symbol,
                type='limit',
                side=order.side,
                price=float(resp['price']),
                amount=float(resp['quantity']),
                filled=float(resp['cum_qty']),
                remaining=float(resp['leaves_quantity']),
                cost=float(resp['average_price']),
                info=resp,
                trades=dict(
                    lastprice= float(resp['LastPrice']),
                    lastquantity= float(resp['LastQuantity']),
                    paidcomission= float(resp['PaidComission']))
            )


setattr(tkgcore.exchanges, "twim", TkgTwim)

#!/usr/bin/env python
# encoding: utf-8

import time
from datetime import timedelta as td
from datetime import datetime as dt
from decimal import Decimal

import grpc

import orders
from base import TwimClientBase, trade, reference, market, private, EMPTY

class Credentials(grpc.AuthMetadataPlugin):
    FIELD = 'api-key'
    def __init__(self, key):
        self.api_key = key

    def __call__(self, context, callback):
        metadata = ((self.FIELD, self.api_key),)
        callback(metadata, None)


class Stream(object):
    def __init__(self, Request, opener, sub, unsub):
        self.opener = opener
        self.Request = Request
        self.sub = sub
        self.unsub = unsub
        self.stack = []

        self.__is_open = False

    @property
    def __queue(self):
        while 1:

            if self.__is_open:
                self.send(self.sub())

            while self.stack:
                msg = self.stack.pop(0)
                yield msg

            if not self.__is_open:
                break
            
    def open(self):
        self.__is_open = True
        for each in self.opener(self.__queue):
            yield each

    def send(self, params):
        request = self.Request(**params)
        self.stack.append(request)

    def close(self):
        self.__is_open = False
        self.send(self.unsub())


class TwimClient(object):

    local_order_book = dict()
    
    local_orders = dict()
    local_orders_index = dict()

    def __init__(self, host, account_id, key, fmt=Decimal, latency=1.):
        self.__account_id = account_id
        self.__balance = {}
        self.__fmt = fmt
        self.__latency = latency

        __creds = Credentials(key)

        __call_creds = grpc.metadata_call_credentials(__creds)
        __ssl_creds = grpc.ssl_channel_credentials()

        __channel_creds = grpc.composite_channel_credentials(__ssl_creds, __call_creds)
        __channel = grpc.secure_channel(host, __channel_creds)

        self.trade_stub = trade.TradeServiceStub(__channel)
        self.reference_stub = reference.ReferenceDataServiceStub(__channel)
        self.market_stub = market.MarketDataServiceStub(__channel)
        self.private_stub = private.PrivateDataServiceStub(__channel)

    @property
    def fmt(self):
        return self.__fmt 
    
    @property
    def AccountId(self):
        return self.__account_id

    def __SecuritySubscribeRequest(self, SecurityId, Subscribe, Request):
        subscribe = Subscribe(SecurityId = SecurityId)
        request = Request(Subscribe = subscribe)
        while 1:
            yield request
            time.sleep(self.__latency)

    def oneof (self, obj, one):
        return obj.HasField(one)
    
    #### twim.reference 

    #region Currencies
    
    def __getCurrencies(self):
        '''
        Получить список базовых валют
        и 
        Построение индекса валют
        '''
        _currencies = {}
        Currencies = self.reference_stub.GetCurrencies(EMPTY).Currencies
        for _currencie in Currencies:
            _currencies[_currencie.Id] = dict(
                Id = _currencie.Id,
                Symbol = _currencie.Symbol,
	            Type = TwimClientBase.getCurrencyType(_currencie.Type)
            )
        return _currencies

    def getCurrencies(self):
        return self.__getCurrencies()

    def getCurrency(self, currency_id):
        '''
        Получение валюты
        '''
        currencies = self.__getCurrencies()
        return currencies.get(currency_id)

    #endregion Currencies
    #region SecurityType

    def __getSecurities(self):
        '''
        Получить список торговых пар
        '''
        return self.reference_stub.GetSecurities(EMPTY)
    
    def getSecurities(self, symbol=None):
        '''
        Cписок торговых хэш по имени пары
        '''
        __index = {}
        for each in self.__getSecurities().Securities:
            __index[each.Symbol] = dict(
                Id=each.Id,
                Symbol=each.Symbol,
                Type= TwimClientBase.getSecurityType(each.Type), 
                BaseCurrencyId=each.BaseCurrencyId,
                TargetCurrencyId=each.TargetCurrencyId
            )
        return __index.get(symbol) if symbol else __index
    #endregion 

    #### twim.private_data 
    
    #region Accounts
    def getAccounts(self):
        '''
        Получить список аккаунтов пользователя
        '''
        __index ={}
        for each in self.private_stub.GetAccounts(EMPTY).Accounts:
            __index[each.Id] = dict(
                Id=each.Id, 
                Type=TwimClientBase.getAccountType(each.Type)
            )
        return __index
    #endregion
    #region Ballance

    def balanceStreamSubscribeRequest(self):
        cls = private.twim_dot_private__data__pb2.BalanceSubscribeRequest
        return dict(Subscribe = cls(AccountId = self.AccountId))

    def balanceStreamUnsubscribeRequest(self):
        cls = private.twim_dot_private__data__pb2.BalanceUnsubscribeRequest
        return dict(Unsubscribe = cls(AccountId = self.AccountId))
    
    def OpenBalanceStream(self):
        '''
        Получение апдейтов по изменению баланса аккаунта
        '''
        stream = Stream(
            private.twim_dot_private__data__pb2.BalanceRequest,
            self.private_stub.OpenBalanceStream,
            self.balanceStreamSubscribeRequest,
            self.balanceStreamUnsubscribeRequest
        )
        return stream
    
    def __calcBallance(self, balances):
        '''
        Подготовка изменений
        '''
        result = {}
        for balance in balances:
            currency = dict(
                self.getCurrency(balance.CurrencyId), 
                amount=self.__fmt(TwimClientBase.DecimalDecode(balance.Amount)), 
                reserve=self.__fmt(TwimClientBase.DecimalDecode(balance.Reserve))
            )
            result[balance.CurrencyId] = currency 
        return result
    
    def setBallance(self, balances):
        '''
        В случае успешной подписки пользователю придёт сообщение BalanceSnapshot,
        содержащее снапшот его баланса
        '''
        self.__balance = self.__calcBallance(balances)
    
    def updateBallance(self, balances):
        '''
        после чего начнут приходить сообщения BalanceUpdate , 
        содержащие новые балансы по одной или нескольким валютам
        '''
        __increment = self.__calcBallance(balances)
        self.__balance.update(__increment)         
    
    def getBallance(self):
        '''
        Получение баланса
        '''
        return self.__balance
    #endregion


    #### twim.market_data
    def OpenTickersStream(self, symbol):
        '''
        Получение обновлений тикера по какому-то инструменту
        '''
        security = self.getSecurities(symbol=symbol)

        Subscribe = market.twim_dot_market__data__pb2.TickerSubscribeRequest
        Request = market.twim_dot_market__data__pb2.TickerRequest
        __subscribe_result = self.__SecuritySubscribeRequest(security['Id'], Subscribe, Request)
        for each in self.market_stub.OpenTickersStream(__subscribe_result):
            yield each
            time.sleep(self.__latency)

    
    #region DOM
    def _applyDOMUpdateCommand_Add(self, side, price, quantity):
        side.setdefault(price, 0)
        side[price] += quantity
    
    def _applyDOMUpdateCommand_Replace(self, side, price, quantity):
        side.setdefault(price, 0)
        side[price] = quantity

    def _applyDOMUpdateCommand_Delete(self, side, price, quantity):
        side.setdefault(price, 0)
        del side[price]

    def __applyDOM(self, store, updates):
        for each in updates:
            price = TwimClientBase.DecimalDecode(each.Price)
            quantity = TwimClientBase.DecimalDecode(each.Quantity)
            command_name = TwimClientBase.GetCommandName(each.Command)
            fn_name = "_applyDOM{}".format(command_name)
            _side_name = TwimClientBase.GetSideName(each.Side)
            _side = 'bids' if _side_name == 'Side_Buy' else 'asks' if _side_name == 'Side_Sell' \
                else 0
            getattr(self, fn_name)(
                store[_side], 
                self.__fmt(price), 
                self.__fmt(quantity)
            )

    def __OpenDepthOfMarketStream(self, symbol):
        '''
        Получение стаканов по какому-то инструменту
        '''
        security = self.getSecurities(symbol=symbol)

        Subscribe = market.twim_dot_market__data__pb2.DOMSubscribeRequest
        Request = market.twim_dot_market__data__pb2.DOMRequest
        __subscribe_result = self.__SecuritySubscribeRequest(security['Id'], Subscribe, Request)

        for each in self.market_stub.OpenDepthOfMarketStream(__subscribe_result):
            yield each
            time.sleep(self.__latency)
    
    def OpenDepthOfMarketStream(self, symbol):
        security = self.getSecurities(symbol=symbol)

        store = self.local_order_book.setdefault(security['Symbol'], dict(bids={}, asks={}))

        for each in self.__OpenDepthOfMarketStream(symbol):
            if each.Update.Updates:
                self.__applyDOM(store, each.Update.Updates)
            elif each.Snapshot.Updates:
                self.__applyDOM(store, each.Snapshot.Updates)

            yield dict(
                bids=sorted(list(store['bids'].items()), key=lambda x: x[0], reverse=True),
                asks=sorted(list(store['asks'].items()), key=lambda x: x[0], reverse=False),
                timestamp=int(time.time() * 1000),
                date=dt.utcnow().strftime("%Y-%m-%dT%H:%M:%S.%fZ")
            )
    
    def getDepthOfMarket(self, symbol):
        store = self.local_order_book.get(symbol)
        return dict(
                bids=sorted(list(store['bids'].items()), key=lambda x: x[0], reverse=True),
                asks=sorted(list(store['asks'].items()), key=lambda x: x[0], reverse=False),
                timestamp=int(time.time() * 1000),
                date=dt.utcnow().strftime("%Y-%m-%dT%H:%M:%S.%fZ")
            )

    #endregion

    def OpenCandlesStream(self, symbol):
        '''
        Получение обновлений по свечкам
        '''
        security = self.getSecurities(symbol=symbol)

        Subscribe = market.twim_dot_market__data__pb2.CandleSubscribeRequest
        Request = market.twim_dot_market__data__pb2.CandleRequest
        __subscribe_result = self.__SecuritySubscribeRequest(security['Id'], Subscribe, Request)
        for each in self.market_stub.OpenCandlesStream(__subscribe_result):
            yield each
            time.sleep(self.__latency)
    

    def GetCandles(self, symbol, From, To, period):
        '''
        Запрос свечек за какой-то период времени
        '''
        security = self.getSecurities(symbol=symbol)

        Request = market.twim_dot_market__data__pb2.CandlesRequest
        
        period = TwimClientBase.GetCandlePeriodNumber('CandlePeriod_Period%s' % period)
        request = Request(SecurityId = security['Id'])
        
        request.Period = period
        request.From.FromDatetime(From)
        request.To.FromDatetime(To)
        
        return self.market_stub.GetCandles(request)

    #twim.trade
    def OpenOrdersStream(self):
        '''
        Получение списка активных оредеров
        '''
        stream = Stream(
            trade.twim_dot_trade__pb2.OrderRequest,
            self.trade_stub.OpenOrdersStream,
            self.orderStreamSubscribeRequest,
            self.orderStreamUnsubscribeRequest
        )
        return stream
        
    def orderStreamSubscribeRequest(self):
        cls = trade.twim_dot_trade__pb2.OrderSubscribeRequest
        return dict(Subscribe = cls(AccountId = self.AccountId))

    def orderStreamUnsubscribeRequest(self):
        cls = trade.twim_dot_trade__pb2.OrderUnsubscribeRequest
        return dict(Unsubscribe = cls(AccountId = self.AccountId))

    def orderStreamPlaceRequest(self, symbol, side, price, quantity, TimeInForceName='GTC', ClientOrderId=None):
        
        security = self.getSecurities(symbol=symbol)

        cls = trade.twim_dot_trade__pb2.NewOrderSingleRequest
        params = dict(
            ClientOrderId = ClientOrderId,
            AccountId = self.AccountId,
            SecurityId = security['Id'],
            OrderType = 1,
            Side = TwimClientBase.GetSideNumber(side),
            Price = TwimClientBase.DecimalEncode(price),
            Quantity = TwimClientBase.DecimalEncode(quantity),
            TimeInForce = TwimClientBase.GetTimeInForceNumber(TimeInForceName)
        )
        return dict(PlaceSingle = cls(**params))

    def orderStreamCancelRequest(self, OrderId=None, ClientOrderId=None):
        cls = trade.twim_dot_trade__pb2.OrderCancelRequest
        params = dict(AccountId=self.AccountId)

        if not OrderId and not ClientOrderId:
            raise UserWarning(' not OrderId and ClientOrderId')

        if OrderId:
            params['OrderId'] = int(OrderId)
        
        if ClientOrderId:
            params['ClientOrderId'] = int(ClientOrderId)

        return dict(Cancel = cls(**params))
    
    def orderStreamStatusRequest(self, OrderId=None, ClientOrderId=None):
        cls = trade.twim_dot_trade__pb2.OrderStatusRequest
        params = dict(AccountId=self.AccountId)

        if not OrderId and not ClientOrderId:
            raise UserWarning(' not OrderId and ClientOrderId')

        if OrderId:
            params['OrderId'] = int(OrderId)
        
        if ClientOrderId:
            params['ClientOrderId'] = int(ClientOrderId)

        return dict(Status = cls(**params))

    def registerOrder(self, order):
        self.local_orders_index[order.client_order_id] = order.order_id
        self.local_orders[order.order_id] = order

    def getOrder(self, client_order_id=None, order_id=None):
        if not order_id and not client_order_id:
            raise UserWarning(' client_order_id or order_id must be sended ')
        
        order_id = order_id or (client_order_id and self.local_orders_index[int(client_order_id)]) or None

        if not order_id:
            raise UserWarning(' order not found ')
        
        return self.local_orders[int(order_id)]

    def getOrders(self):
        return dict(orders = self.local_orders, index=self.local_orders_index)

    ORDER_BY_CREATE_DT = 1
    ORDER_BY_MODIFICATION_DT = 2

    def HistoricalOrders(self, Count=5, Offset=0, Order=None):
        '''
        Получение истории ордеров
        '''
        request = trade.twim_dot_trade__pb2.HistoricalOrdersRequest()
        request.AccountId = self.AccountId
        request.Count = Count
        request.Offset = Offset
        request.Order = Order or TwimClient.ORDER_BY_CREATE_DT

        for each in self.trade_stub.GetHistoricalOrders(request).OrderStates:
            yield orders.OrderState(each, fmt=self.__fmt) 
    
    def loadOrders(self, **kwargs):
        for order in self.HistoricalOrders(**kwargs):
            self.registerOrder(order)


def main():
    tw = TwimClient('beta.twim.trade:8946', 46, '2GYLM48X6Q', fmt=float)
    symbol = "ICX/BTC"

    # print (tw.GetCurrencies())
    # print (tw.GetSecurities())
    # print (tw.GetAccounts())
    
    # stream = tw.OpenBalanceStream()
    # for each in stream.open():
    #     if tw.oneof(each, 'BalanceSnapshot'):
    #         tw.setBallance(each.BalanceSnapshot.Balances)
    #     elif tw.oneof(each, 'BalanceUpdate'):
    #         tw.updateBallance(each.BalanceUpdate.Balances)
    #     print (tw.getBallance())

    # for each in tw.HistoricalOrders():
        # print(each)

    # stream = tw.OpenOrdersStream()
    # orders = [
    #     tw.orderStreamPlaceRequest(symbol, 'buy', 123, 5, ClientOrderId=2000100501),
    #     tw.orderStreamPlaceRequest(symbol, 'sell', 120, 31, ClientOrderId=2000100502)
    # ]

    # for each in stream.open():
                
        # if orders:
            # stream.send(orders.pop(0))
        
        # if tw.oneof(each, 'NewOrderSingleResponse'):
        #     tw.orderStreamCancelRequest(
        #         each.NewOrderSingleResponse.ClientOrderId, 
        #         each.NewOrderSingleResponse.OrderId
        #     )
        #     stream.close()

        # print (each)
    #
    # now = dt.now()
    # cnd = tw.GetCandles(symbol, now - td(minutes=30), now, '1m')
    # print(cnd, cnd.Candles)
    #
    # OpenTickersStream
    # for each in tw.OpenTickersStream(symbol):
        # print (each)
    
    # OpenDepthOfMarketStream
    step = next(tw.OpenDepthOfMarketStream(symbol))
    print ("bids:{}".format(step['bids']))
    print ("asks:{}".format(step['asks']))
    # for each in tw.OpenDepthOfMarketStream(symbol):
        # print (each)
    
    # OpenCandlesStream
    # for each in tw.OpenCandlesStream(symbol):
    #     print (each.Candle)  

if __name__ == '__main__':
    main()

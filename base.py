#!/usr/bin/env python
# encoding: utf-8

import imp
from datetime import datetime as dt
from decimal import Decimal

from twim.common_pb2 import Decimal as _Decimal
from twim.common_pb2 import DESCRIPTOR as common_descriptor


trade = imp.load_source('twim.trade_pb2_grpc', 'twim/twim.trade_pb2_grpc.py')
reference = imp.load_source('twim.reference_data_pb2_grpc', 'twim/twim.reference_data_pb2_grpc.py')
market = imp.load_source('twim.market_data_pb2_grpc', 'twim/twim.market_data_pb2_grpc.py')
private = imp.load_source('twim.private_data_pb2_grpc', 'twim/twim.private_data_pb2_grpc.py')


RejectReason = trade.twim_dot_trade__pb2.RejectReason.DESCRIPTOR
OrderStatus = trade.twim_dot_trade__pb2.OrderStatus.DESCRIPTOR
TimeInForce = trade.twim_dot_trade__pb2.TimeInForce.DESCRIPTOR
CurrencyType = reference.twim_dot_reference__data__pb2.CurrencyType.DESCRIPTOR
SecurityType = reference.twim_dot_reference__data__pb2.SecurityType.DESCRIPTOR
AccountType = private.twim_dot_private__data__pb2.AccountType.DESCRIPTOR
EMPTY = reference.google_dot_protobuf_dot_empty__pb2.Empty()


class TwimClientBase(object):
    
    @staticmethod
    def TimestampDecode(val):
        return dt.utcfromtimestamp(val.seconds)

    @staticmethod
    def RejectReasonName(val):
        return RejectReason.values_by_number.get(val).name.replace('RejectReason_','').lower()

    @staticmethod
    def OrderStatusName(val):
        return OrderStatus.values_by_number.get(val).name.replace('OrderStatus_','').lower()

    @staticmethod
    def DecimalDecode(value):
        return Decimal(value.Significand) / pow(10, value.Exponent)
    
    @staticmethod
    def DecimalEncode(value):
        return _Decimal(Significand=int(float(value) * 100000000), Exponent=8)

    TIME_IN_FORCE = {
        'GTC':'TimeInForce_GTC',
        'IOC':'TimeInForce_IOC',
        'FOK':'TimeInForce_FOK',
    }
    
    @staticmethod
    def GetTimeInForceNumber(name):
        name = TwimClientBase.TIME_IN_FORCE.get(name, name)
        return  TimeInForce.values_by_name[name].number

    @staticmethod
    def GetTimeInForceName(value):
        return TimeInForce.values_by_number.get(value).name.replace('TimeInForce_','')

    # 1-buy 2-sell
    SIDE_NAMES = {
        'buy':'Side_Buy',
        'sell':'Side_Sell'
    }

    @staticmethod
    def GetSideNumber(name):
        name = TwimClientBase.SIDE_NAMES.get(name, name)
        return common_descriptor.enum_types_by_name['Side'].values_by_name.get(name).number

    @staticmethod
    def GetSideName(number):
        return common_descriptor.enum_types_by_name['Side'].values_by_number.get(number).name

    @staticmethod
    def GetCandlePeriodNumber(name):
        return  market.twim_dot_market__data__pb2.CandlePeriod.DESCRIPTOR.values_by_name[name].number

    @staticmethod
    def GetCommandName(number):
        return market.twim_dot_market__data__pb2.UpdateCommand.DESCRIPTOR.values_by_number[number].name

    @staticmethod
    def getCurrencyType(number):
        return CurrencyType.values_by_number.get(number).name
    
    @staticmethod
    def getSecurityType(number):
        return SecurityType.values_by_number.get(number).name
    
    @staticmethod
    def getAccountType(number):
        return AccountType.values_by_number.get(number).name

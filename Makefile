install:
	make pip_install
	make build

build:
	python -m grpc_tools.protoc -I./protos --python_out=. --grpc_python_out=. protos/*.proto

debug:
	GRPC_VERBOSITY=debug GRPC_TRACE=all python client.py

pip_install:
	pip install -r requirements.txt

pip_reload:
	pip freeze | xargs pip uninstall -y
	make pip_install
import tkgcore
import tkg_twim
import datetime
import time

twim_wrapper = tkgcore.ccxtExchangeWrapper.load_from_id("twim")  # type: tkg_twim.TkgTwim
time.sleep(5)
# balance = twim_wrapper.fetch_free_balance()
# print(balance)
# markets = twim_wrapper.get_markets()
# print(markets)
# print('*********************************************')
pair = "ICX/BTC"
qv = 1 # Quote Volume
steps = 1 # Init amount of steps on each side of ob
buyamount = 0.1
sellamount = 0.1
trading_fees = 0.001 # !
qs = 0.03 # Quote Spread in percent
data = []

def loop():
    ob1 = tkg_twim.TwimClient.local_order_book[pair]
    ask1 = float(next(iter(ob1["asks"].keys())))
    bid1 = float(next(iter(ob1["bids"].keys())))

    midprice1 = (ask1 + bid1)/2
    # myinterval = {ask: }
    # myaskinterval = [ask1, ]
    # mybidinterval =
    spread1 = (bid1 - ask1)/midprice1
    spread1p = 100 * spread1
    print('[{}]: {} ask={} bid={} spread={}%'.format(
          datetime.datetime.utcnow().strftime("%H:%M:%S.%f"),
          pair,
          ask1,
          bid1,
          spread1p))
    init_order = (sellamount * bid1) - (buyamount * ask1)
    print("Purchase amount {}".format(init_order))
    print("Cost of trade {}".format((((sellamount * b) + (buy * a)) * trading_fees)))
    print('Profit is {}'.format(((sellamount * b) - (buy * a)) - (((sellamount * b) + (buy * a)) * trading_fees)))
    profit = ((sellamount * b) - (buy * a)) > (((sellamount * b) + (buy * a)) * trading_fees)
    print(((sellamount * b) - (buy * a)) > (((sellamount * b) + (buy * a)) * trading_fees))
    time.sleep(0.1)

    while (sell * b - buy * a) > (((sell * b) + (buy * a)) * trading_fees):

        ob1 = tkg_twim.TwimClient.local_order_book[pair]
        a = int(next(iter(ob1["asks"].keys())))
        b = int(next(iter(ob1["bids"].keys())))
        data.append((((sell * b) - (buy * a)) - (((sell * b) + (buy * a)) * trading_fees)))
        print('[{}]: {} ask={} bid={} spread={}%'.format(
            datetime.datetime.utcnow().strftime("%H:%M:%S.%f"),
            pair,
            a,
            b,
            spread1p))

        print("Purchase amount {}".format(((sell * b) - (buy * a))))
        print("Cost of doing business {}".format((((sell * b) + (buy * a)) * trading_fees)))
        print('Profit is {}'.format(((sell * b) - (buy * a)) - (((sell * b) + (buy * a)) * trading_fees)))
        print(((sell * b) - (buy * a)) > (((sell * b) + (buy * a)) * trading_fees))

        if ((sell * b - buy * a) > (((sell * b) + (buy * a)) * trading_fees)) is False:
            break

    print('We should have had {} potential trades'.format(len(data) * 2))
    print(data)

# for each in tkg_twim.TwimClient.local_order_book:
#     print(int(next(iter(each[symbol]["asks"].values()))))
# # print(order_resp)

def restart():
    while True:
        try:

            loop()

        except Exception as e:
            print(e)
            time.sleep(0.1)

        time.sleep(0.2)

        print(' ')
        print('*********************************************')
        print('+++++++++++++++++++++++++++++++++++++++++++++')
        print(' ')

if __name__ == '__main__':
    restart()

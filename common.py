#!/usr/bin/env python
# encoding: utf-8

from datetime import datetime
from decimal import Decimal
import json
from concurrent.futures import ThreadPoolExecutor
import concurrent.futures.thread
from functools import partial


class Executor(object):
    def __init__(self, loop=None, nthreads=1):
        self.__ex = ThreadPoolExecutor(nthreads)
        self._loop = loop
        
    def __call__(self, f, *args, **kw):
        return self._loop.run_in_executor(self.__ex, partial(f, *args, **kw))

    def shutdown(self):
        self.__ex._threads.clear()
        concurrent.futures.thread._threads_queues.clear()


class Encoder(json.JSONEncoder):

    def default(self, obj):
        if isinstance(obj, datetime):
            return obj.isoformat()
        if isinstance(obj, Decimal):
            return float(obj)
        if hasattr(obj, 'toJson'):
            return obj.toJson()
        return json.JSONEncoder.default(self, obj)
        
def dumps(obj):
    return json.dumps(obj, cls=Encoder)
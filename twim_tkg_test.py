import tkgcore
import tkg_twim
import datetime
import time

twim_wrapper = tkgcore.ccxtExchangeWrapper.load_from_id("twim")  # type: tkg_twim.TkgTwim
time.sleep(5)
# balance = twim_wrapper.fetch_free_balance()
# print(balance)
# markets = twim_wrapper.get_markets()
# print(markets)

symbol = "ICX/BTC"

# for i in range(1):
# time.sleep(0.01)
# i += 1
ob1 = tkg_twim.TwimClient.local_order_book
ask1 = next(iter(ob1[symbol]["asks"].keys()))
bid1 = next(iter(ob1[symbol]["bids"].keys()))
midprice1 = (ask1 + bid1)/2
spread1 = (bid1 - ask1)/midprice1
spread1p = 100 * spread1
print('\r[{}]: {} ask={} bid={} spread={}%'.format(
      datetime.datetime.utcnow().strftime("%Y-%m-%dT%H:%M:%S.%fZ"),
      symbol,
      ask1,
      bid1,
      spread1p))
print('Creating order: {}, buy, 0.01, {}'.format(symbol, bid1))
orderprice = ask1*99/100
create_resp = twim_wrapper._create_order(symbol, 'whxc', 'buy', 0.01, orderprice)
print('Order sended to the stream and next response catched as first response:')
print(create_resp)
print("Waiting for 0.1 sec...")
time.sleep(0.1)
cancelorder = tkgcore.TradeOrder("", symbol, 0, "")
cancelorder.id = create_resp['id']
print('Canceling order with order_id:{}'.format(cancelorder.id))
cancel_resp = twim_wrapper._cancel_order(cancelorder)
print(cancel_resp)
print("Waiting for 0.5 sec...")
time.sleep(0.5)
fetchres = twim_wrapper._fetch_order_by_order_id(cancelorder.id)
print(fetchres)
# for each in tkg_twim.TwimClient.local_order_book:
#     print(int(next(iter(each[symbol]["asks"].values()))))


twim_wrapper.execute.shutdown()

exit(1)

import tkgcore
import tkg_twim
import datetime
import time

twim_wrapper = tkgcore.ccxtExchangeWrapper.load_from_id("twim")  # type: tkg_twim.TkgTwim
time.sleep(5)
# balance = twim_wrapper.fetch_free_balance()
# print(balance)
# markets = twim_wrapper.get_markets()
# print(markets)

symbol = "ICX/BTC"

for i in range(10000):
    time.sleep(0.01)
    ob1 = tkg_twim.TwimClient.local_order_book[symbol]
    ask1 = next(iter(ob1["asks"].keys()))
    bid1 = next(iter(ob1["bids"].keys()))
    midprice1 = (ask1 + bid1)/2
    spread1 = (bid1 - ask1)/midprice1
    spread1p = 100 * spread1
    print('\r[{}]: {} ask={} bid={} spread={}%'.format(
          datetime.datetime.utcnow().strftime("%Y-%m-%dT%H:%M:%S.%fZ"),
          symbol,
          ask1,
          bid1,
          spread1p),
            end="")

# print('Creating order: {}, buy, 0.01, {}'.format(symbol, bid1))
# order_resp = twim_wrapper._create_order(symbol, 'whxc', 'buy', 0.01, bid1)
# print('Order sended to the stream and next response catched as first response:')
# print(order_resp)
# for each in tkg_twim.TwimClient.local_order_book:
#     print(int(next(iter(each[symbol]["asks"].values()))))


twim_wrapper.execute.shutdown()

exit(1)

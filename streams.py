#!/usr/bin/env python
# encoding: utf-8

from client import TwimClient
import orders


def BalanceStream(client: TwimClient):
    stream = client.OpenBalanceStream()
    for each in stream.open():
        if client.oneof(each, 'BalanceSnapshot'):
            client.setBallance(each.BalanceSnapshot.Balances)
        elif client.oneof(each, 'BalanceUpdate'):
            client.updateBallance(each.BalanceUpdate.Balances)


def DepthOfMarketStream(client: TwimClient, symbol: str):
    for client in client.OpenDepthOfMarketStream(symbol):
        pass


def OrderStream(client: TwimClient):
    '''
    OrderStream events:
    OrderSubscribeResponse
    SubscribeResponse = 1;
    SnapshotBegin = 2;
    SnapshotEnd = 3;

    NewOrderSingleResponse = 4;
    NewOrderSingleReject = 5;
    OrderReplaceResponse = 6;
    OrderReplaceReject = 7;
    OrderCancelResponse = 8;
    OrderCancelReject = 9;
    OrderStatusResponse = 10;
    OrderStatusReject = 11;
    ExecutionSingleReport = 12;
    HeartBeat = 13;
    '''
    stream = client.OpenOrdersStream()
    setattr(client, 'order_stream', stream)
    for each in stream.open():
        if client.oneof(each, 'OrderStatusResponse'):
            # print('OrderStatusResponse')
            client.registerOrder(orders.OrderStatusResponse(each.OrderStatusResponse, fmt=client.fmt))
        elif client.oneof(each, 'OrderStatusReject'):
            # print('OrderStatusReject')
            client.registerOrder(orders.OrderStatusReject(each.OrderStatusReject, fmt=client.fmt))
        elif client.oneof(each, 'NewOrderSingleResponse'):
            # print('NewOrderSingleResponse')
            client.registerOrder(orders.NewOrderSingleResponse(each.NewOrderSingleResponse, fmt=client.fmt))
        elif client.oneof(each, 'NewOrderSingleReject'):
            # print('NewOrderSingleReject')
            client.registerOrder(orders.NewOrderSingleReject(each.NewOrderSingleReject, fmt=client.fmt))
        elif client.oneof(each, 'OrderReplaceResponse'):
            # print('OrderReplaceResponse')
            client.registerOrder(orders.OrderReplaceResponse(each.OrderReplaceResponse, fmt=client.fmt))
        elif client.oneof(each, 'OrderReplaceReject'):
            # print('OrderReplaceReject')
            client.registerOrder(orders.OrderReplaceReject(each.OrderReplaceReject, fmt=client.fmt))
        elif client.oneof(each, 'OrderCancelResponse'):
            # print('OrderCancelResponse')
            client.registerOrder(orders.OrderCancelResponse(each.OrderCancelResponse, fmt=client.fmt))
        elif client.oneof(each, 'OrderCancelReject'):
            # print('OrderCancelResponse')
            client.registerOrder(orders.OrderCancelReject(each.OrderCancelReject, fmt=client.fmt))
        elif client.oneof(each, 'ExecutionSingleReport'):
            # print('ExecutionSingleReport')
            client.registerOrder(orders.ExecutionSingleReport(each.ExecutionSingleReport, fmt=client.fmt))

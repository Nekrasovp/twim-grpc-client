#!/usr/bin/env python
# encoding: utf-8

from base import TwimClientBase
from decimal import Decimal


class Order(object):
    """
    Основной класс всех ордеров
    + int64 ClientOrderId = 1;
	+ int64 OrderId = 2;
    """
    def __init__(self, proto, fmt=Decimal):
        self.__fmt = fmt
        self.client_order_id = proto.ClientOrderId
        self.order_id = proto.OrderId
        self.extendStatus(proto)

    def extendStatus(self, proto):
        pass
    
    def extendResult(self, result):
        return result

    def toJson(self):
        return self.extendResult(dict(
            client_order_id = self.client_order_id,
            order_id = self.order_id
        ))
    
    def __repr__(self):
        return str(self.toJson())


class OrderProcess(Order):
    """
    Базовый класс всех ордеров
    + google.protobuf.Timestamp Timestamp = 16;
    """
    def __init__(self, proto, fmt=Decimal):
        super(OrderProcess, self).__init__(proto, fmt=fmt)
        self.timestamp = TwimClientBase.TimestampDecode(proto.Timestamp) # google.protobuf.Timestamp

    def toJson(self):
        result = super(OrderProcess, self).toJson()
        result.update(dict(
            timestamp = self.timestamp
        ))
        return result   

class OrderReject(OrderProcess):
    """
    + RejectReason RejectReason = 2;
    """
    def __init__(self, proto, fmt=Decimal):
        super(OrderReject, self).__init__(proto, fmt=fmt)
        self.reject_reason = TwimClientBase.RejectReasonName(proto.RejectReason)  # RejectReason

    def toJson(self):
        result = super(OrderReject, self).toJson()
        result.update(dict(
            reject_reason = self.reject_reason
        ))
        return result


class OrderStateTrait(object):
    """
    Примесь для всех ордеров
    
    // Order status
	OrderStatus OrderStatus;
	int32 AccountId;
	int32 SecurityId;
	OrderType OrderType;
	twim.common.Side Side;
	twim.common.Decimal Price;
	twim.common.Decimal Quantity;
	TimeInForce TimeInForce;
	twim.common.Decimal LeavesQuantity;
	twim.common.Decimal CumQty;
	google.protobuf.Timestamp CreationDate;
	google.protobuf.Timestamp ModificationDate;
    """
    def extendStatus(self, proto):
        
        self.order_status = TwimClientBase.OrderStatusName(proto.OrderStatus)  # OrderStatus
        
        self.account_id = proto.AccountId
        self.security_id = proto.SecurityId

        self.order_type = proto.OrderType # OrderType
        self.side = TwimClientBase.GetSideName(proto.Side)  # twim.common.Side
        
        self.price = TwimClientBase.DecimalDecode(proto.Price) # twim.common.Decimal
        self.quantity = TwimClientBase.DecimalDecode(proto.Quantity) # twim.common.Decimal

        self.TimeInForce = TwimClientBase.GetTimeInForceName(proto.TimeInForce)

        self.leaves_quantity = TwimClientBase.DecimalDecode(proto.LeavesQuantity) # twim.common.Decimal
        self.cum_qty = TwimClientBase.DecimalDecode(proto.CumQty) # twim.common.Decimal
        
        self.dt_create = TwimClientBase.TimestampDecode(proto.CreationDate) # google.protobuf.Timestamp
        self.dt_modification = TwimClientBase.TimestampDecode(proto.ModificationDate) # google.protobuf.Timestamp
	
    def extendResult(self, result):
        result.update(dict(
            order_status  = self.order_status,
            account_id = self.account_id,
            security_id = self.security_id,
            order_type = self.order_type,
            side = self.side,
            price = self.price,
            quantity = self.quantity,
            time_in_force = self.TimeInForce,
            leaves_quantity = self.leaves_quantity,
            cum_qty = self.cum_qty,
            dt_create = self.dt_create,
            dt_modification = self.dt_modification
        ))
        return result

class OrderStateTradeTrait(OrderStateTrait):
    """
    Примесь для всех ордеров с торгов
    twim.common.Decimal AveragePrice;
    """
    def extendStatus(self, proto):
        super(OrderStateTradeTrait, self).extendStatus(proto)
        self.average_price = TwimClientBase.DecimalDecode(proto.AveragePrice) # twim.common.Decimal

    def extendResult(self, result):
        result = super(OrderStateTradeTrait, self).extendResult(result)
        result.update(
            average_price = self.average_price
        )
        return result


class OrderState(OrderStateTrait, Order):
    """
    для HistoricalOrdersResponse
    """
    pass


class NewOrderSingleResponse(OrderStateTradeTrait, OrderProcess):
    pass


class NewOrderSingleReject(OrderStateTradeTrait, OrderReject):
    pass


class OrderReplaceResponse(OrderStateTradeTrait, OrderProcess):
    pass


class OrderReplaceReject(OrderStateTradeTrait, OrderReject):
    """
    // Request parameters
	ModificationMode ModificationMode;
	twim.common.Decimal RequestedPrice;
	twim.common.Decimal RequestedQuantity;
    """
    def __init__(self, proto, fmt=Decimal):
        super(OrderReplaceReject, self).__init__(proto, fmt=fmt)
        self.requested_quantity = TwimClientBase.DecimalDecode(proto.RequestedQuantity)
        self.requested_price = TwimClientBase.DecimalDecode(proto.RequestedPrice)
        self.modification_mode = proto.ModificationMode

    def toJson(self):
        result = super(OrderReplaceReject, self).toJson()
        result.update(dict(
            requested_quantity = self.requested_quantity,
            requested_price = self.requested_price,
            modification_mode = self.modification_mode
        ))
        return result


class OrderCancelResponse(OrderStateTradeTrait, OrderProcess):
    pass


class OrderCancelReject(OrderStateTradeTrait, OrderReject):
    pass


class OrderStatusResponse(OrderStateTradeTrait, OrderProcess):
    pass


class OrderStatusReject(OrderReject):
    pass
    

class ExecutionSingleReport(Order):
    """
    int64 TradeId;
	twim.common.Decimal LastPrice;
	twim.common.Decimal LastQuantiy;
	twim.common.Decimal PaidComission;
	bool IsTaker;
    """
    def __init__(self, proto, fmt=Decimal):
        super(ExecutionSingleReport, self).__init__(proto, fmt=fmt)
        self.trade_id = proto.TradeId
        self.last_price = TwimClientBase.DecimalDecode(proto.LastPrice)
        self.last_quantiy = TwimClientBase.DecimalDecode(proto.LastQuantiy)
        self.paid_comission = TwimClientBase.DecimalDecode(proto.PaidComission)
        self.is_taker = proto.IsTaker

    def toJson(self):
        result = super(ExecutionSingleReport, self).toJson()
        result.update(dict(
            trade_id=self.trade_id,
            last_price=self.last_price,
            last_quantiy=self.last_quantiy,
            paid_comission=self.paid_comission,
            is_taker=self.is_taker
        ))
        return result
